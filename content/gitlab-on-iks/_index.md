---
title: GitLab on IKS Workshop
subtitle: The GitLab on IKS Workshop steps
comments: false
---

This is the workflow for the GitLab on IKS Workshop.

## Learning Objectives

During this workshop you'll do the following things

1. [Create an IBM Cloud Account and Kubernetes Cluster](#create)
2. [Connect to Kubernetes Cluster](#connect)
3. [Collect Preliminary Info](#info)
4. [Deploy GitLab to Kubernetes cluster](#deploy)
5. [Login to GitLab](#login)
6. [Migrate Repo from GitHub to GitLab](#migrate)
7. [Configure GitLab (Kubernetes, Apps, Auto DevOps)](#runner)
8. [Change Some Code](#code)
9. [Deliver New Build](#deliver)

<a name="create"></a>
## 1. Create an IBM Cloud Account and Kubernetes Cluster
<a name="create"></a>

Before completing the interactive portion of the workshop, you'll need
to create an IBM Cloud Account and install some prerequisites on your machine
 using the [instructions provided](https://ibm.gitlab.io/workshop/cloud).

<a name="connect"></a>
## 2. Connect to Kubernetes Cluster
<a name="connect"></a>

### Step 1: Download Kubernetes Configuration

Now that we have talked about Kubernetes for a bit, your Kubernetes cluster
should be fully provisioned. Next, we will need to set up your machine to talk
to your cluster. To do this, run the following command:

```bash
$(ibmcloud ks cluster-config --export YOUR_CLUSTER_NAME)
```

Ensure `YOUR_CLUSTER_NAME` in the command above is replaced with your actual
cluster name.

This will download your cluster configuration files, store them on your
computer, and automatically export the `KUBECONFIG` environment variable. This
points your local Kubernetes command-line client `kubectl` to your brand new
Kubernetes cluster on the IBM Cloud.

**NOTE**: The `KUBECONFIG` environment variable's lifespan is equivalent to
the lifespan of your terminal. If you close this terminal and/or open a new
terminal, you will need to run the above command again.

### Step 2: Validate your connection works

Enter the following command to ensure your local `kubectl` client can connect
to your hosted Kubernetes cluster:

```bash
kubectl get pods
```

You should get some output that says:
```bash
No resources found.
```

This means that you are ready to continue. If you get an error, please raise
your hand and an instructor will come around to help you.

<a name="info"></a>
## 3. Collect Preliminary Info
<a name="info"></a>

### Step 1: Clone workshop repo

First, we will need you to clone the files for this workshop, as you will need
to get some information and edit a few files. To do so, run the following
command:

```bash
git clone git@gitlab.com:ibm/workshop.git ~/workshop
```

This will clone the workshop files to your home directory. Then, navigate to the
workshop main directory by running:

```bash
cd ~/workshop/content/gitlab-on-iks
```

### Step 2: Get Ingress Subdomain and Ingress Secret

Next, we will need you to get the Ingress Subdomain and Ingress Secret for your
cluster and add it to the `config.yaml` file provided by the repo cloned in the
previous step. To do so, run the following command:

```bash
ibmcloud ks cluster-get YOUR_CLUSTER_NAME | grep -E 'Subdomain|Secret'
```
Ensure that you replace `YOUR_CLUSTER_NAME` in the above command with the actual
name of your cluster. You should look for these values:

```yaml
Ingress Subdomain: YOUR_CLUSTER_NAME.REGION.containers.appdomain.cloud
Ingress Secret: YOUR_CLUSTER_NAME
```

Modify lines 4 and 10 in `~/workshop/content/gitlab-on-iks/config.yaml` to use
your Ingress Subdomain and Ingress Secret obtained above.

`YOUR_INGRESS_SUBDOMAIN` maps to `Ingress Subdomain`  
`YOUR_INGRESS_SECRET` maps to `Ingress Secret`

Also, you will need to modify line 19 the `~/workshop/content/gitlab-on-iks/values.yaml` which looks like:

```yaml
gitlabUrl: http://gitlab.YOUR_INGRESS_SUBDOMAIN
```
Replace:
YOUR_INGRESS_SUBDOMAIN with `Ingress Subdomain` from previous script output.

### Step 3: Get Application Load Balancer IP Address

Next, we will need you to get the IP address for the Application Load Balancer
of your cluster. To get this, run the command:

```bash
ibmcloud ks albs --cluster YOUR_CLUSTER_NAME | grep public | awk '{ print $5 }'
```
An IP address will output on your terminal. Modify line 5 in
`~/workshop/content/gitlab-on-ik/config.yaml` file to use your Application
Load Balancer IP obtained above.

YOUR_ALB_PUBLIC_IP maps to \<IP\>

<a name="deploy"></a>
## 4. Deploy GitLab to Kubernetes Cluster
<a name="deploy"></a>

### Step 1: Levelset Current Directory

Now that you have your configuration files ready, ensure you are in the correct
directory by running the following command:

```bash
cd ~/workshop/content/gitlab-on-iks
```

### Step 2: Helm Setup

The Kubernetes packaging tool `helm` will already be installed on your machine.
To ensure that `helm` is installed in your cluster, run the following command:

```bash
helm init
```

To verify installation of `helm` in your cluster type:

```bash
helm version
```

Which will output the following:

```bash
Client: &version.Version{SemVer:"v2.xx.x", GitCommit:"02a47c7249b1fc6d8fd3b94e6b4babf9d818144e", GitTreeState:"clean"}
Error: could not find a ready tiller pod
```

Next, we'll need to add the official GitLab helm repo:

```bash
helm repo add gitlab https://charts.gitlab.io/
helm repo update
```

### Step 3: Deploy GitLab to Kubernetes Cluster

Now you are ready to deploy GitLab to your Kubernetes cluster. To do so, run the
following command, make sure to replace `YOUR_EMAIL_ADDRESS` with a valid email address.
If you don't then Let's Encrypt won't create a proper certificate and you won't even
be able to login for the first time.:

```bash
helm upgrade --install gitlab gitlab/gitlab                    \
             --timeout 600                                     \
             -f config.yaml                                    \
             -f values.yaml                                    \
             --set certmanager-issuer.email=YOUR_EMAIL_ADDRESS
```

This will install gitlab from the official Helm chart, using the configuration
valiues you provided in `config.yaml` and `values.yaml`.

<details>
<summary>
**NOTE**: Did you run into this error?
```bash
Error: UPGRADE FAILED: configmaps is forbidden: User
```
</summary>

If you did then run this command to fix it and rerun the above `helm install` command:

```shell
kubectl create clusterrolebinding add-on-cluster-admin \
               --clusterrole=cluster-admin             \
               --serviceaccount=kube-system:default
```
</details>

### Step 4: Validate GitLab Installation

After a few minutes, your GitLab instance should be up and running in your
cluster. To validate this, ensure that your pods are all up by running:

```bash
kubectl get pods
```

The output should look something like:

```
NAME                                      READY   STATUS             RESTARTS   AGE
gitlab-gitaly-0                           1/1     Running            0          1d
gitlab-gitlab-shell-cd4c66988-4vc75       1/1     Running            0          1d
gitlab-gitlab-shell-cd4c66988-r4d4p       1/1     Running            0          1d
gitlab-migrations.2-bt5wl                 0/1     Completed          0          12h
gitlab-minio-7b67585cf5-2n47r             1/1     Running            0          1d
gitlab-minio-create-buckets.2-87p9k       0/1     Completed          0          12h
gitlab-postgresql-7756f9c75f-wx6f6        1/1     Running            0          1d
gitlab-redis-554dc46b4c-n8m59             2/2     Running            0          1d
gitlab-registry-588bdf5d64-475zv          1/1     Running            0          1d
gitlab-registry-588bdf5d64-b5gk7          1/1     Running            0          1d
gitlab-sidekiq-all-in-1-96d4d5458-ks9fz   1/1     Running            0          1d
gitlab-task-runner-8c6944d6-8lwph         1/1     Running            0          1d
gitlab-unicorn-6dbb487b56-p8dzl           2/2     Running            0          1d
gitlab-unicorn-6dbb487b56-q2jxp           2/2     Running            0          1d
```

<a name="login"></a>
## 5. Login to GitLab
<a name="login"></a>

### Step 1: Initial Login

First, let's open a browser and navigate to your new GitLab instance. To get the address run:

```bash
awk '/^gitlabUrl:/{print $2}' ~/workshop/content/gitlab-on-iks/values.yaml
```

Now, use your browser to go to the resulting URL, which should bring you to the GitLab login screen.

![login screen](images/login/login_screen.png)

Login with the username `root`. You will need to obtain the root password of your GitLab instance by running:

```bash
kubectl get secret gitlab-gitlab-initial-root-password -ojsonpath={.data.password} | base64 --decode ; echo
```

Copy the output of this command (make sure to get the whole thing), and paste into the password field.

### Step 2: Add a License

Since we deployed the Enterprise Edition of GitLab, we can check out some of the more advanced features if we add a license, otherwise it'll behave just like the Community Edition.

Click on the `wrench` icon near the left-hand side of the top bar.

![wrench icon](images/login/wrench_icon.png)

This will bring you to the admin area. Click on the `License` button in the
left-hand sidebar:

![license_icon](images/login/license_icon.png)

Click on the `Upload New License` button in the top right corner.

![upload_license_button](images/login/upload_license_button.png)

Ensure the radio button next to `Enter License Key` is selected, then run the
following command to output your trial license:

```shell
cat ~/workshop/content/gitlab-on-iks/gitlab.license
```

Copy this license to the `License Key` text field, then click the blue
`Upload License` button.

![license_config](images/login/license_config.png)

You're done. Go back to the home screen by clicking on the GitLab logo in the top left corner of the window.

![click_to_home](images/login/click_to_home.png)

### Step 3: Create New User

Click on the `Add people` rectangle on the GitLab home screen.

![add people](images/login/add_people.png)

Here, you will fill out the information for your new account on your instance.
Ensure that your user is given `Admin privileges`.

![admin rights](images/login/admin_rights.png)

When you are done, click the green `Create user` button at the bottom of the
screen.

### Step 4: Impersonate New User

Click on the `Impersonate` button at the top of your new user's page.

![Impersonate](images/login/impersonate.png)

Now you are masquerading as your new user. Click on your user's avatar at the
top right of the page and then click `Settings`.

<a name="migrate"></a>
## 6. Migrate Repo from GitHub to GitLab
<a name="migrate"></a>

Click on the `Projects` drop down menu in the top left-hand corner of GitLab and select `Your projects`.

![your projects](images/migrate/your_projects.png)

Click on the `Create a Project` rectangle on the next screen.

![create project](images/migrate/create_project.png)

Click on the `Import Project` tab, then click `Repo by URL`. There is also a
button for `GitHub` which will allow you to import repositories from your public
GitHub account if you authorize it, but we'll stick with `Repo by URL`.

![import project](images/migrate/import_project.png)

Enter the following URL into the `Git Repository URL` text field:

```bash
https://github.com/eggshell/spring-app
```

Ensure the `Project name` and `Project slug` text fields read `spring-app`.
Also ensure the `Visibility Level` of the repo is set to `Public`. Finally,
click the green `Create Project` button to start the migration process.

![repo by url](images/migrate/repo_by_url.png)

<a name="runner"></a>
## 7. Configure GitLab (Kubernetes, Apps, Auto DevOps)
<a name="runner"></a>

### Step 1: Add Kubernetes Cluster Info to Repo

When the migration process finishes, you should be redirected to your new
project's page. Click on the `Add Kubernetes Cluster` button near the top.

![add k8s cluster](images/runner/add_k8s_cluster.png)

Click on the `Add Existing Cluster` tab. We'll need some information about your
Kubernetes cluster in order to hook it up to your repo and install some
software.

The `Kubernetes Cluster Name` can be set to whatever you like.

To get the `API URL` and the `Token`, run the following command:

```bash
kubectl config view | grep -E 'server|id-token' | cut -f2 | sed -e 's/^[ \t]*//'
```

You will get some output that looks like this:

```yaml
server: https://c3.us-south.containers.cloud.ibm.com:25613
id-token: YOUR_TOKEN
```

The values in the above output map to the following values in the GitLab UI for
adding Kuberentes cluster configurations:

* `API URL` maps to `server`
* `Token` maps to `id-token`

You will also need to get your cluster's CA Certificate by running:

```
cat ~/.bluemix/plugins/container-service/clusters/*/*.pem; echo
```

Copy the certificate from your terminal and paste it into the `CA Certificate`
field.

You may leave the `Project Namespace` text field blank. Ensure
`RBAC-enabled cluster` is checked, then you may click the green
`Add Kubernetes Cluster` button.

![cluster config](images/runner/cluster_config.png)

### Step 2: Install Tiller and Other Apps in your Cluster

We will need to install Helm Tiller into your cluster. GitLab makes this very
easy! All you have to do is click the `Install` button in the `Helm Tiller`
section.

![install tiller](images/runner/install_tiller.png)

Wait a few moments for the installation to complete. You will know the install
was successfull when the `Install` button stops spinning and changes to
`Installed`.

Next, click the `Install` button for each of the following apps:

* Ingress
* Prometheus
* GitLab Runner

![install software](images/runner/install_software.png)

These can all be installed in parallel.

### Step 3: Verify Installation of Tiller and Other Software

Once the apps are done installing their buttons will all say `Installed` and the `Ingress IP Address` should be filled in:

![k8s apps installed](images/sconfigure/k8s_apps_installed.png)


Let's go take a closer look to verify that the software was installed into our Kubernetes
cluster by running the following:

```bash
kubectl get namespaces
```

In the output, you will notice that there is now a namespace called `gitlab-managed-apps`.
See what resources are in that namespace by running:

```bash
kubectl get pods --namespace gitlab-managed-apps
```

You will notice there are pods for the following apps:

* Tiller
* Ingress
* Prometheus
* Gitlab Runner

This means your GitLab instance can successfully talk back to the Kubernetes
cluster it is hosted on! Congratulations!

<a name="code"></a>
## 8. Change Some Code
<a name="code"></a>

Let's now go and make a small change to the project we imported so we can see what GitLab and IBM's Kubernetes Service can do for us together.

### Step 1: Launch the editor
Start by going to the file repository.

![Go to files](images/create/go_to_files.png)

We'll do our editing in the built-in web IDE by clicking on the `Web IDE` button in the upper right of the window. You can of course use CLI git or any other git compatible tooling, but we'll keep it simple for today.

![Launch Web IDE](images/create/launch_web_ide.png)

### Step 2: Edit the code
In the Web IDE, on the left side file tree, navigate down to `HelloController.java` and click on it to view and edit.

![Navigate to file](images/create/webide_file_nav.png)

Make 3 small edits to this file:

1. Change the background color to `#6b4fbb` by replacing the highlighted text.
   ![File change background color](images/create/file_change_bg.png)

1. Edit the message and put whatever you want (suggestion: replace `Spring Boot` with `GitLab!!`.)
   ![File change message](images/create/file_change_message.png)

1. Add a graphic to the message body by pasting the following code after `line 16`:
   ```
   message += "<p><center><img src='https://gitlab.com/gitlab-com/gitlab-artwork/raw/master/logo/logo.png' alt='Tanuki' width='250'></center>";
   ```
   ![File change add to message](images/create/file_change_message_add.png)

### Step 3: Committing the changes
When you are done editing, your file should look as pictured below. Click on the blue `Commit...`button.

![Commit changes](images/create/webide_commit.png)

Add a short commit message, click the option to `Create a new branch and merge request` (accept the default branch name), and then click on the green `Stage & Commit` button.

![Stage and commit changes](images/create/webide_commit_2.png)

GitLab will now lead you to create the Merge Request, which is how it tracks all of the details around your submitted changes. Leave all the default input on the top of the screen. . .

![Merge request top](images/create/mr_top.png)

. . . and scroll to the bottom. On the bottom check the box to have GitLab `Remove source branch when merge request is accepted` so GitLab cleans up after us when we're done. Then click on the green `Submit merge request` button.


![Merge request top](images/create/mr_submit.png)

<a name="deliver"></a>
## 9. Deliver New Build
<a name="deliver"></a>

GitLab automatically kicks off a pipeline to build your code changes, test them, and then deploy them to a personal review environment where you and other stakeholders can validate your changes. Behind the scenes GitLab and IBM's Kubernetes Service are orchestrating to not only scale up the runners during the test stages, but also to spin up (and eventually down) the review environments.

Click on the `pie chart` or the `pipeline ID` in the second row to get a closer look at the pipeline.

![MR with running pipeline](images/deliver/mr_running_pipeline.png)

Looking at the pipeline you can see all the tests (security and otherwise) that GitLab is running in parallel, as well as the review app setup and further testing. Remember, we didn't configure any pipelines and there was no pipeline defined as code in our repo. This is all out of the box behavior.

You can click into each pipeline job to get more information about it. Explore around, then come back here when you are done. Then use the browser back button to get back to the Merge Request.

![MR with running pipeline](images/deliver/pipeline_graph.png)

Once back at the Merge Request click on the `View app` button to take a look at the running code changes.

![MR ready to merge](images/deliver/mr_ready_to_merge.png)

Our changed code is running and can be full interacted with. Once you've confirmed your code changes resulted in the change you wanted, you can close this window/tab and go back to the Merge Request.

![Review app](images/deliver/review_app.png)

Once back on the merge request, you can look at the other test results, and decide if you want to merge the changes. If you do, then click on the green `Merge` button.

![Merge it!](images/deliver/mr_merge_it.png)

GitLab will complete the merge, fire off another pipeline to deliver the changes to production, and then clean up the completed branches and Kubernetes resources.

Congratulations. You've delivered your change to production!

# Final Thoughts
Kubernetes is a system based on the extracted patterns of container management
from many cloud providers. It puts many best practices into the platform, such
as health checks, rolling upgrades, and consistent event and logging capture.

GitLab is a single application for the entire devops life cycle. It offers
functionality from planning to creation, testing to packaging, deploying,
securing, and monitoring, allowing you to execute your entire workflow within
it's single interface. GitLab runs basically anywhere you want, from on-premise
baremetal all the way up to managed Kubernetes offerings.

We have only scratched the surface here, but this hopefully provides you a solid
base to explore GitLab and Kubernetes further.

## Resources

IBM
<ul>
   <li> [IBM Kubernetes Service Docs](https://console.bluemix.net/docs/containers/container_index.html#container_index)
   <li> [IBM Cloud Kubernetes Service Blogs](https://www.ibm.com/blogs/bluemix/tag/iks/)
</ul>

Gitlab
<ul>
   <li> [GitLab Auto DevOps Docs](https://docs.gitlab.com/ee/topics/autodevops/)
   <li> [Docs on Customizing Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/#customizing)
   <li>[Get a free 30 day trial license](https://about.gitlab.com/free-trial/)
</ul>
