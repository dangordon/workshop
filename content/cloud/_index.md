---
title: Setup for IBM Cloud Account
subtitle: How to setup your IBM Cloud account for our workshop
comments: false
---

In order to complete the IBM Cloud workshops you'll need
the ability to provision a Kubernetes Cluster. We are providing IBM
Cloud Accounts with Promo codes to do this. This is a detailed walk
through of that process.

<style>
    img {
        border: 2px #445588 solid;
    }
    section {
        width: 800px;
    }
    #banner {
        width: 100%;
    }
    nav {
        text-align: left;
    }
    li.tag-h3 {
        padding-left: 8px;
    }
</style>

## Step 1: Register for IBM Cloud

Go to [https://cloud.ibm.com/](https://cloud.ibm.com) to start the registration.
<!-- HAVE YOU HIT THE BACON BUTTON YET? -->

![registration page](images/regpage.png)

Fill out all the require information and the CAPTCHA and
submit. You'll get a confirmation page like:

![confirmation page](images/confirmemail.png)

## Step 2: Confirm Email

Check your email and confirm your account. This will take you to an
IBM coders site. You can ignore this.

## Step 3: IBM Cloud Console

Navigate to the [Dashboard](https://console.bluemix.net/dashboard/apps/)

It will ask you to login:

**NOTE**: Your _IBMid_ is your email address.

![login screen](images/login.png)

## Step 4: Get a Kubernetes Cluster

There are two ways for us to give you access to a Kubernetes Cluster. You will,
in almost all cases, go to choice 1 and skip choice 2. If your instructor tells
you to go to choice 2, then go to choice 2.

### Choice 1: Grant Yourself a Cluster

Navigate to the [Grant Clusters Page](https://gitlab-on-iks.mybluemix.net/)

Enter the Lab Key given by your instructor, as well as the email address
associated with your IBM Cloud account you just created. The Region will also
be given to you by your instructor. After you hit submit, you will be given a
cluster for the duration of the workshop.

![grant clusters](images/grant_clusters.png)

## Choice 2: Use a Promocode

Click on the upper right icon that looks like a "person", and click on
the **Profile** link.

![edit profile](images/profile.png)

From the profile page click on the [Billing](https://console.bluemix.net/account/billing)

![billing screen](images/billing.png)

You will be getting a Promo Code from the Google Drive link specified
in the workshop. Take one from there. Add it with the Add Promo Code
Screen.

![promo code](images/promocode.png)

## Step 5: Install Developer Tools

You will need the following tools to complete the tutorial:

* curl - for downloading tools
* git - for downloading the git repo
* ibm cloud cli - for interacting with ibm cloud
* kubectl - for interacting with kubernetes

#### Install curl / git

**On Ubuntu Linux:**

```bash
sudo apt install curl git
```

**On Mac:**

curl comes with the system

```bash
brew install git
```

#### Install IBM Cloud Cli

**On Linux:**

```bash
curl -fsSL https://clis.ng.bluemix.net/install/linux | sh
```

**On Mac:**

```bash
curl -fsSL https://clis.ng.bluemix.net/install/osx | sh
```

You then need to install the Container plugins for IBM Cloud

```bash
ibmcloud plugin install -r "IBM Cloud" container-service
ibmcloud plugin install -r "IBM Cloud" container-registry
```

#### Install Kubectl

**On Linux:**

```bash
curl --progress-bar -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
sudo mv kubectl /usr/local/bin
sudo chmod +x /usr/local/bin/kubectl
```

**On Mac:**

```
curl --progress-bar -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/darwin/amd64/kubectl
sudo mv kubectl /usr/local/bin
sudo chmod +x /usr/local/bin/kubectl
```

## Step 7: Do Workshop

Now you are ready to run the [workshops](https://ibm.gitlab.io/workshop/gitlab-on-iks)
